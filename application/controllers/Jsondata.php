<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Login
 * 
 * @author Bernardo Pinheiro Camargo
 * @since 2019
 */
class Jsondata extends MY_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function __construct() {
    	parent::__construct();
		$this->load->model('Usuarios_model');    	
	}

	public function index(){
		print_r('ada');die;
		$this->template->set("container", 'login');
	}

    public function getrepos(){
        
        $curl = curl_init();
        $agent = $_SERVER['HTTP_USER_AGENT'];

        curl_setopt_array($curl, array(
        CURLOPT_URL => 'https://api.github.com/user/repos?per_page=100',
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => '',
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 0,
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => 'GET',
        CURLOPT_HTTPHEADER => array(
            'Accept: application/vnd.github+json',
            'Authorization: Bearer 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',
            'User-Agent: '.$agent

        ),
        ));

        $response = curl_exec($curl);

        curl_close($curl);
        $res = json_decode($response, true);
        $result = array();
        foreach ($res as $value) {
            array_push($result, $value);
        }

        $res = [
            'status'   => 'success',
            'code'     => '1',
            'data' 		 => $result
        ];

        header('Content-Type: application/json');
		echo json_encode($res);
		exit;
	}

	/**
	 * login
	 *
	 * Tenta realizar o login e redireciona para a tela inicial
	 * 
	 * @return void
	 */
	public function login(){
		$this->load->model('Usuarios_model');    	

		$post = $this->input->post();

		if($this->guard->login($post['email'], $post['senha'])){
			$this->session->set_flashdata('success', 'Bem vindo!');
		}
		else
			$this->session->set_flashdata('error', 'Credenciais inválidas.');
	
		redirect(site_url(),'location');
	}

	/**
	 * logout
	 *
	 * Faz o logout e redireciona para a tela inicial
	 * 
	 * @return void
	 */
	public function logout(){
		$this->guard->logout();

		redirect(site_url(),'location');
	}
}