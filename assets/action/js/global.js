$(document).ready(function() {
    getrepos()
    function getrepos(){
        $.ajax({
            type: 'get',
            dataType: 'json',
            url: 'data/getrepos',
            success: function(result){
                const data = result.data
                var num = Math.ceil(data.length / 6);
                var element = ''
                var element_ = ''
                const array = [1, 2, 3, 4, 5, 6, 7, 8]; 
                const chunk = 4; 

                $('#HomeBanner').slick({
                    dots: true,
                    lazyLoad: 'progressive',
                    autoplay: true,
                    autoplaySpeed: 8000,
                    arrow:true
                });

                splitIntoChunk(data, 6);
                function splitIntoChunk(arr, chunk) { 
                    for (i=0; i < arr.length; i += chunk) {
                        let lement = `<div class="slide dark">
                                            <div class="container">
                                                <div class="row">`
                            
                            let tempArray = ''; 
                            tempArray = arr.slice(i, i + chunk); 
                            // console.log(tempArray); 
                            let col1 = '<div class="col-md-6">'
                                let part1 = tempArray.slice(0,3)
                                part1.forEach(element => {
                                    col1 += `<div class="hobby">
                                                <div class="hobby-image">
                                                    <div class="bg-image-container"><img src="assets/img/photos/hobby03.jpg" alt=""></img></div>
                                                </div>
                                                <div class="hobby-image-hover">
                                                    <div class="bg-image-container"><img src="assets/img/photos/hobby03-hover.jpg" alt=""></img></div>
                                                </div>
                                                <div class="hobby-body">
                                                    <div class="hobby-content">
                                                        <h4 class="hobby-title" style="color:#58a6ff;">${element.name} ${element.private ? '<span class="badge-private">private</span>' : '<span class="badge-public">public</span>'}</h4>
                                                        <span class="hobby-caption">${element.language ? element.language : '-'} </span>
                                                    </div>
                                                    <button onclick="window.open('https://github.com/benosons','_blank')" class="hobby-btn btn btn-github btn-sm"><i class="fa fa-github"></i>Check my Github</button>
                                                </div>
                                            </div>`
                                });
                            col1 +='</div>'

                            let col2 = '<div class="col-md-6">'
                                let part2 = tempArray.slice(3,6)
                                part2.forEach(element => {
                                    col2 += `<div class="hobby">
                                                <div class="hobby-image">
                                                    <div class="bg-image-container"><img src="assets/img/photos/hobby03.jpg" alt=""></img></div>
                                                </div>
                                                <div class="hobby-image-hover">
                                                    <div class="bg-image-container"><img src="assets/img/photos/hobby03-hover.jpg" alt=""></img></div>
                                                </div>
                                                <div class="hobby-body">
                                                    <div class="hobby-content">
                                                        <h4 class="hobby-title" style="color:#58a6ff;">${element.name} ${element.private ? '<span class="badge-private">private</span>' : '<span class="badge-public">public</span>'}</h4>
                                                        <span class="hobby-caption">${element.language ? element.language : '-'}</span>
                                                    </div>
                                                    <button onclick="window.open('https://github.com/benosons','_blank')" class="hobby-btn btn btn-github btn-sm"><i class="fa fa-github"></i>Check my Github</button>
                                                </div>
                                            </div>`
                                });
                            col2 +='</div>'

                            lement += col1
                            lement += col2

                            lement += `</div>
                                            </div>
                                                <div>`
                        // element += lement
                        // element_ += lement_
                        $('#HomeBanner').slick('slickAdd', lement);

                    }
                } 

                // $('.section-slides').append(element)
                // $('.section-bgs').append(element_)
            }
        })
    }
    
    
    
})